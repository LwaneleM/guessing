﻿using GuessingGame.Application;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System;

namespace GuessingGame.Website.Pages
{
    public class IndexModel : PageModel
    {
        private readonly IGameRepository _gameRepository;
        private readonly string _gameIdSessionKey = "gameId";

        public string InputBoxMessage { get; private set; }
 
        public string InputBoxTitle { get; private set; }
        
        public int GuessLeft { get; private set; }
        public int Hintleft { get; private set; }

        public IndexModel(IGameRepository gameRepository)
        {
            _gameRepository = gameRepository;
        }
        
        public void SetView(IGame game) 
        {
            GuessLeft = game.RemainingGuessAttempts;
            Hintleft = game.RemainingHintAttempts;
        }

        public void OnGet()
        {
            if (HttpContext.Session.IsAvailable)
            {
                InputBoxTitle = "Play Game";
                InputBoxMessage = "Number has been generated. Try Guessing it!";
            }

            var game = GetGameState();
            SetView(game);
        }

        public void OnPostHints()
        {
            InputBoxTitle = "HINT";
            var game = GetGameState();

            if (game.HasHints())
            {
                InputBoxMessage = game.GetHint();
            }
            else
            {
                InputBoxMessage = UserFeedback.NoHintsLeft();
            }
            SetView(game);
            _gameRepository.SaveGame(Guid.Parse(HttpContext.Session.GetString(_gameIdSessionKey)), game);
        }

        public void OnPostGuess()
        {
            InputBoxTitle = "ANSWER";

            var game = GetGameState();

            if (game.HasGuesses())
            {
                
                string playerGuess = Request.Form["getInput"];
                InputBoxMessage = FormInputHandler.CheckUserInput(ValidateUserInput.IsAnswerValid(playerGuess), game);
            }
            else
            {
                InputBoxMessage = UserFeedback.OffMessege();
                game.ResetGame();
            }

            SetView(game);
            _gameRepository.SaveGame(Guid.Parse(HttpContext.Session.GetString(_gameIdSessionKey)), game);
        }

        private IGame GetGameState()
        {
            if (string.IsNullOrEmpty(HttpContext.Session.GetString(_gameIdSessionKey)))
            {
                var gameData = _gameRepository.CreateGame();
                HttpContext.Session.SetString(_gameIdSessionKey, gameData.Item1.ToString());
                return gameData.Item2;
            }
            else
            {
                var gameId = _gameRepository.GetGame(Guid.Parse(HttpContext.Session.GetString(_gameIdSessionKey)));
                return gameId;
            }
        }
    }
}
