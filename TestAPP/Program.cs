﻿using System;
using System.Data.SqlClient;
using System.Text.Json;

namespace TestAPP
{
    public class gamedto
    {
        public int finalNumber { get; set; }
        public int remainingAttempts { get; set; }
        public int remainingHints { get; set; }

    }
    class Program
    {
        
        public static void addNewData(Guid ID, string name)
        {
           
            using (var sqlConnection = new SqlConnection(@"Data Source=(LocalDb)\MSSQLLocalDB;Initial Catalog=GuessingGameDatabase;Integrated Security=True"))
            {
                sqlConnection.Open();
                
                Console.WriteLine($"the ID is " + ID);
                string Command = "INSERT INTO dbo.GameDatabase (GameID,GameData) VALUES (@ID,@name);";
                using (var sqlCommand = new SqlCommand(Command, sqlConnection))
                {
                    sqlCommand.Parameters.AddWithValue("@ID", ID);
                    sqlCommand.Parameters.AddWithValue("@name", name);
                    sqlCommand.ExecuteNonQuery();

                    Console.WriteLine("Data is added");
                }
            }
        }

        public static void DeleteData() 
        {
           
            using (var sqlConnection = new SqlConnection(@"Data Source=(LocalDb)\MSSQLLocalDB;Initial Catalog=GuessingGameDatabase;Integrated Security=True")) 
            {
                sqlConnection.Open();
                using (var command = new SqlCommand("DELETE dbo.GameDatabase", sqlConnection)) 
                {
                    command.ExecuteNonQuery();
                }
            }
        }

        public static void addGame() 
        {

        }

        public static void ViewData()
        {
    
            using (var sqlConnection = new SqlConnection(@"Data Source=(LocalDb)\MSSQLLocalDB;Initial Catalog=GuessingGameDatabase;Integrated Security=True"))
            {
                sqlConnection.Open();
                using (var command = new SqlCommand("SELECT GameID,GameData FROM dbo.GameDatabase",sqlConnection))
                {
                    using (var reader = command.ExecuteReader())
                    {

                        Console.WriteLine("___________________________");
                        Console.WriteLine("GameID      |    GameData");
                        Console.WriteLine("___________________________");
                        while (reader.Read())
                        {
                            for (int i = 1; i < reader.FieldCount; i++)
                            {
                                Console.WriteLine("-" + reader.GetValue(i-1)+"  "+ reader.GetValue(i));
                            }
                          
                        }
                        Console.WriteLine("___________________________");
                    }
                }
            }
        }
        public static string getData()
        {
            Console.WriteLine("Type a command:");
            return Console.ReadLine();
        }
        static void Main(string[] args)
        {
            //string connection = "Data Source=(LocalDb)\MSSQLLocalDB;Initial Catalog=GuessingGameDatabase;Integrated Security=True";
            string name;
            while (true) 
            {
                Console.WriteLine("Please enter your name");
                name = Console.ReadLine();
                if (name == "add")
                {
                    Console.WriteLine("Please enter your data");
                    gamedto game = new gamedto();
                    game.finalNumber = 10;
                    game.remainingAttempts = 3;
                    game.remainingHints = 5;
                    string namey = JsonSerializer.Serialize(game);
                    Guid ID = Guid.NewGuid();
                    addNewData(ID,namey);
                }

                if (name == "view")
                {
                    ViewData();
                    
                }

                if (name == "delete")
                { 
                    DeleteData();
                }

                if (name == "exit") 
                {
                    break;
                }
            }
           
            Console.WriteLine("Program complete");
            

        }
    }
}
