﻿namespace GuessingGame.Application.Api.Controllers.RestartGame
{
    public class Restart
    {
        public bool IsRestarted { get; set; }
    }
}
