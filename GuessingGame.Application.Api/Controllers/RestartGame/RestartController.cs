﻿using Microsoft.AspNetCore.Mvc;
using System;

namespace GuessingGame.Application.Api.Controllers.RestartGame
{
    [Route("api/game")]
    [ApiController]
    public class RestartController : ControllerBase
    {
        private readonly IGameRepository _gameRepository;

        public RestartController(IGameRepository gameRepository) 
        {
            _gameRepository = gameRepository;
        }
        [Route("{gameId}/restart")]
        [HttpDelete]
        public ActionResult RestartGame(Guid gameId) 
        {
            _gameRepository.RestartGame(gameId);
            return Ok();
        }
    }
}
