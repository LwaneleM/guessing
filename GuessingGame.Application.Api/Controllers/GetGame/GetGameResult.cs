﻿using System;

namespace GuessingGame.Application.Api.Controllers.GetGame
{
    public class GetGameResult
    {
        public bool IsPlaying { get; set; }
        
        public int HintsRemaining { get; set; }

        public int GuessesRemaining { get; set; }

        public Guid GameId { get; set; }
    }
}
