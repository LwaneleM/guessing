﻿using Microsoft.AspNetCore.Mvc;
using System;

namespace GuessingGame.Application.Api.Controllers.GetGame
{
    [Route("api/game")]
    [ApiController]
    public class GetGameController : ControllerBase
    {
        private readonly IGameRepository _gameRepository;
  
        public GetGameController(IGameRepository gameRepository) 
        {
            _gameRepository = gameRepository;
        }

        [HttpGet]
        public ActionResult<GetGameResult> Execute(Guid id) 
        {
            var game = _gameRepository.GetGame(id);
            return new GetGameResult
            {
                GuessesRemaining = game.RemainingGuessAttempts,
                HintsRemaining = game.RemainingHintAttempts,
                IsPlaying = game.IsPlaying,
                GameId = id
                
            };
        }
    }
}
