﻿using Microsoft.AspNetCore.Mvc;
using System;

namespace GuessingGame.Application.Api.Controllers.Hint
{
    [Route("api/game")]
    [ApiController]
    public class HintController : ControllerBase
    {
        private readonly IGameRepository _gameRepository;
        public HintController(IGameRepository gameRepository)
        {
            _gameRepository = gameRepository;
        }
        [Route("{gameId}/hint")]
        [HttpPost]
        public ActionResult<HintOutput> Execute(Guid gameId)
        {
            var game = _gameRepository.GetGame(gameId);
            var message = game.HasHints() ? game.GetHint() : UserFeedback.NoHintsLeft();
            _gameRepository.SaveGame(gameId,game);

            return new HintOutput
            {
                HintMessage = message
            };
        }

    }
}
