﻿namespace GuessingGame.Application.Api.Controllers.Hint
{
    public class HintOutput
    {
        public string HintMessage { get; set; }
    }
}
