﻿namespace GuessingGame.Application.Api.Controllers.Guess
{
    public class GuessInput
    {
        public int Answer { get; set; }
    }
}
