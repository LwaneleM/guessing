﻿namespace GuessingGame.Application.Api.Controllers.Guess
{
    public class GuessOutput
    { 
        public AnswerState Message { get; set; }
    }
}
