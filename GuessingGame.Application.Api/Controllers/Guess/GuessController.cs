﻿using Microsoft.AspNetCore.Mvc;
using System;

namespace GuessingGame.Application.Api.Controllers.Guess
{
    [Route("api/game")]
    [ApiController]
    public class GuessController : ControllerBase
    {
        private readonly IGameRepository _gameRepository;

        public GuessController(IGameRepository gameRepository) 
        {
            _gameRepository = gameRepository;
        }

        [Route("{gameId}/guess")]
        [HttpPost]
        public ActionResult<GuessOutput> GuessInputPost(Guid gameId, GuessInput input)
        {
            var game = _gameRepository.GetGame(gameId);
            var result = game.HasGuesses() ? game.Guess(input.Answer) : AnswerState.OUTOFATTEMPTS;
            /*var result = game.Guess(input.Answer);*/
            _gameRepository.SaveGame(gameId, game);

            return new GuessOutput
            {
                Message = result,
            };
        }
    }
}
