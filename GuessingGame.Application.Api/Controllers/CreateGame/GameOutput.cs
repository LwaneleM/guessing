﻿using System;

namespace GuessingGame.Application.Api.Controllers.CreateGame
{
    public class GameOutput
    {
        public Guid GameId { get; set; }
    }
}
