﻿using GuessingGame.Application.Api.Controllers.CreateGame;
using Microsoft.AspNetCore.Mvc;

namespace GuessingGame.Application.Api.Controllers.tempCreateGame
{
    [Route("api/game")]
    [ApiController]
    public class CreateGameController : ControllerBase
    {
        private IGameRepository _gameRepository;

        public CreateGameController(IGameRepository gameRepository)
        {
            _gameRepository = gameRepository;
        }

        [HttpPost]
        public ActionResult<GameOutput> CreateGame() 
        {
            var gameId = _gameRepository.CreateGame().Id;

            return new GameOutput
            {
                GameId = gameId
            };
        }
    }
}
