using NSubstitute;
using System;
using Xunit;

namespace GuessingGame.Application.Tests
{
    public class InMemoryGameRepositoryTest
    {
        [Fact]
        public void ItShouldCreateAGame()
        {
            //Arrange
            var mockRandomNumber = Substitute.For<IRandomNumberGenerator>();
            var inMemoryGameRepository = new InMemoryGameRepository(mockRandomNumber);
            var game = new Game(mockRandomNumber);
            
            //Act
            game.Initialise(1, 100, 5, 3);
            var result = inMemoryGameRepository.CreateGame();
            
            //Assert
            Assert.Equal(result.Item1.ToString().Length, new Guid().ToString().Length);
            Assert.Equal(result.Item2.RemainingGuessAttempts, game.RemainingGuessAttempts);
            Assert.Equal(result.Item2.RemainingHintAttempts, game.RemainingHintAttempts);
        }

        [Fact]
        public void ItShouldGetTheGame()
        {
            //Arrange
            var mockRandomNumber = Substitute.For<IRandomNumberGenerator>();
            var inMemoryGameRepository = new InMemoryGameRepository(mockRandomNumber);
            var game = new Game(mockRandomNumber);
            var id = new Guid();
            //Act
            game.Initialise(1, 100, 5, 3);
            inMemoryGameRepository.SaveGame(id, game);
            var result = inMemoryGameRepository.GetGame(id);

            //Assert
            Assert.Equal(result, game);
        }

        [Fact]
        public void ItShouldRestartTheGame()
        {
            //Arrange
            var mockRandomNumber = Substitute.For<IRandomNumberGenerator>();
            var inMemoryGameRepository = new InMemoryGameRepository(mockRandomNumber);
            var game = new Game(mockRandomNumber);
            var id = new Guid();
            var expactedGame= new Game(mockRandomNumber);
            expactedGame.Initialise(1, 100, 5, 3);
            //Act
            game.Initialise(1, 100, 2, 3);
            inMemoryGameRepository.SaveGame(id, game);
            inMemoryGameRepository.RestartGame(id);
            var result = inMemoryGameRepository.GetGame(id);

            //Assert
            Assert.NotEqual(result, expactedGame);
        }


    }
}
