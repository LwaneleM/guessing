﻿using System;
using System.Collections.Generic;

namespace GuessingGame.Application
{
    public class InMemoryGameRepository : IGameRepository
    {
        private readonly Dictionary<Guid, IGame> _data;
        private readonly IRandomNumberGenerator _randomNumberGenerator;
        
        public InMemoryGameRepository(IRandomNumberGenerator randomNumberGenerator)
        {
            _data = new Dictionary<Guid, IGame>();
            _randomNumberGenerator = randomNumberGenerator;
        }

        public (Guid, IGame) CreateGame()
        {
            var id = Guid.NewGuid();
            IGame game = new Game(_randomNumberGenerator);
            game.Initialise(1, 100, 5, 3);
            SaveGame(id, game);
            return (id, game);
        }

        public IGame GetGame(Guid id)
        { 
            return _data[id];
        }

        public void RestartGame(Guid id)
        {
            var game = GetGame(id);
            game.Initialise(1, 100, 5, 3);
            SaveGame(id, game);

        }

        public void SaveGame(Guid id, IGame game)
        {
            _data[id] = game;
           
        }

    }
}
