﻿using GuessingGame.Web.DTO;

namespace GuessingGame.Application
{
    public class ValidateUserInput
    {
        public static ValidatedUserInputResult IsAnswerValid(string playerInput)
        {
            if (int.TryParse(playerInput, out _))
            {
                return new ValidatedUserInputResult
                {
                    Response = InputResponse.NUMBER,
                    PlayerAnswer = playerInput
                };
            }
            else 
            {
                return new ValidatedUserInputResult
                {
                    Response = InputResponse.INVALDINPUT,
                    PlayerAnswer = playerInput
                };
               
            }   
            
        }
    }
}
