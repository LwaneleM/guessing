﻿using GuessingGame.Web.DTO;
using System;

namespace GuessingGame.Application
{
    public class FormInputHandler
    {
        public static string CheckUserInput(ValidatedUserInputResult result,IGame game)
        {
            if (game.HasGuesses())
            {
                if (result.Response.Equals(InputResponse.NUMBER))
                {
                    return game.Guess(int.Parse(result.PlayerAnswer)) switch
                    {
                        AnswerState.EQUAL => UserFeedback.WellDoneMessage(),

                        AnswerState.LOW => UserFeedback.TooLowMessage(),

                        AnswerState.HIGH => UserFeedback.TooHighMessage(),

                        _ => throw new InvalidOperationException("Invalid command"),
                    };
                }
                else
                {
                    return $"{UserFeedback.UnrecognizedInput()} <{result.PlayerAnswer}>";
                }
            }
            else
            {
                return $"Sorry you do not have enough guess left  {UserFeedback.OffMessege()}";
            }
           
        }

        
    }
}
