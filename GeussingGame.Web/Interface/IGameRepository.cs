﻿using System;

namespace GuessingGame.Application
{
    public interface IGameRepository
    {
        void SaveGame(Guid id, IGame game);
        
        IGame GetGame(Guid id);
        
        void RestartGame(Guid id);
        
        (Guid Id, IGame Game) CreateGame();
        
    }
}
