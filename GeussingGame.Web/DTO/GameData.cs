﻿using System;

namespace GuessingGame.Application
{
    public class GameData
    {
        public int RemainingGuessAttempts { get; set; }

        public bool IsGameCompleted { get; set; }

        public int RemainingHintAttempts { get; set; }

        public int FinalAnswer { get; set; }

        public bool IsGamePlaying { get; set; }
    }
}
