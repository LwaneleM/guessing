﻿namespace GuessingGame.Web.DTO
{
    public class ValidatedUserInputResult
    {
        public InputResponse Response { get; set; }
        public string PlayerAnswer { get; set; }
    }
}

