﻿using Microsoft.Extensions.Configuration;
using System;
using System.Data.SqlClient;
using System.Text.Json;

namespace GuessingGame.Application
{
    public class SqlGameRepository : IGameRepository
    {
        private readonly IConfiguration _configuration;
        private readonly IRandomNumberGenerator _randomNumberGenerator;

        public SqlGameRepository(IConfiguration configuration, IRandomNumberGenerator randomNumberGenerator)
        {
            _configuration = configuration;
            _randomNumberGenerator = randomNumberGenerator;
        }

        public (Guid, IGame) CreateGame()
        {
            var id = Guid.NewGuid();
            IGame game = new Game(_randomNumberGenerator);
            game.Initialise(1, 100, 5, 3);
            GameData gamedata = new()
            {
                FinalAnswer = game.FinalAnswer,
                RemainingGuessAttempts = game.RemainingGuessAttempts,
                RemainingHintAttempts = game.RemainingHintAttempts
            };
            string gameJson = JsonSerializer.Serialize(gamedata);

            // ExecuteNonQuery for inserting new data in to the database
            string connectionString = _configuration.GetConnectionString("GameDatabase");
            using var sqlConnection = new SqlConnection(connectionString); 
            sqlConnection.Open();
            string Command = "INSERT INTO dbo.Games VALUES (@gameId,@gameData);";
            using var sqlCommand = new SqlCommand(Command, sqlConnection);
            sqlCommand.Parameters.AddWithValue("@gameID", id);
            sqlCommand.Parameters.AddWithValue("@gameData", gameJson);
            sqlCommand.ExecuteNonQuery(); 
            
            return (id, game);
        }

        public IGame GetGame(Guid id)
        {
            //Getting data from the database using the id.
            string connectionString = _configuration.GetConnectionString("GameDatabase");
            using var sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            string Command = "SELECT GameData FROM dbo.Games WHERE GameID = @name;";
            using var sqlCommand = new SqlCommand(Command, sqlConnection);
            sqlCommand.Parameters.AddWithValue("@name", id);
            using var reader = sqlCommand.ExecuteReader();
            reader.Read();
            GameData gameData = JsonSerializer.Deserialize<GameData>(reader.GetString(0));
            IGame game = new Game(_randomNumberGenerator);
            game.RestoreGameState(gameData.RemainingGuessAttempts, gameData.RemainingHintAttempts, gameData.FinalAnswer);
            
            return game;
        }

        public void RestartGame(Guid id)
        {
            var game = GetGame(id);
            game.ResetGame();
            game.Initialise(1, 100, 5, 3);
            SaveGame(id, game);
        }
        
        public void SaveGame(Guid id, IGame game)
        {
            GameData gamedata = new()
            {
                FinalAnswer = game.FinalAnswer,
                RemainingGuessAttempts = game.RemainingGuessAttempts,
                RemainingHintAttempts = game.RemainingHintAttempts
            };
            //Updating the game from the database using the game id 
            string gameJson = JsonSerializer.Serialize(gamedata);
            string connectionString = _configuration.GetConnectionString("GameDatabase");
            using var sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            string Command = "UPDATE dbo.Games SET GameData = @gameData WHERE GameID = @gameID;";
            using var sqlCommand = new SqlCommand(Command, sqlConnection);
            sqlCommand.Parameters.AddWithValue("@gameID", id);
            sqlCommand.Parameters.AddWithValue("@gameData", gameJson);
            sqlCommand.ExecuteNonQuery();
        }
    }
}
