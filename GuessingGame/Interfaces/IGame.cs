﻿using System;

namespace GuessingGame
{
    public interface IGame
    {
        void Initialise(int min, int max, int numberOfAttempts, int numberOfHints);

        bool HasGuesses();

        AnswerState Guess(int answer);

        bool HasHints();

        string GetHint();
        void RestoreGameState(int remainingGuessing, int remainingHintAttempts,
            int finalAnswer);

        int RemainingGuessAttempts { get; }

        int RemainingHintAttempts { get; }

        public bool IsGameCompleted { get; }

        bool IsPlaying { get; }

        int FinalAnswer { get; }
        public void ResetGame();
    }
}
