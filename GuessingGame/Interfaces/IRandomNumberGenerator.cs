﻿namespace GuessingGame
{
    public interface IRandomNumberGenerator
    {
        int GetRandomNumber(int min, int max);
    }
}
