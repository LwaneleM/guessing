﻿

namespace GuessingGame
{
    public static class UserFeedback
    {
        // This mathod display the intro message when the game start
        public static string IntroMessege()
        {
            return "=================IT'S PLAY TIME================== \n" +
                "Hint: 2\nAttempts: 4\nLet the games begin!";
        }

        //This function display in commands that the game understands
        public static string HelpMessage()
        {
            return "== Commands I understand : \n" +
                         "> exit - Shutdown the game\n" +
                         "> Hint - Gives you the hint about the answer\n"+
                         "> Number e.g {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10……}\n" ;
        }

        //This method display information if there are no hint left
        public static string NoHintsLeft()
        {
            return "Sorry there are no hints left";
        }
        
        //This method informs the player that their answer is too high
        public static string TooHighMessage()
        {
            return "Sorry your guess is too high";
        }

        //This method  display how many attempts and hints left
        public static string GameInfo(int hintsLeft, int attemptsLeft){
            return $"Hint : {hintsLeft}\nAttempts : {attemptsLeft}";
        }

        //This method informs the player that their answer is too low
        public static string TooLowMessage()
        {
            return "Sorry your guess is too low";
        }

        //This message is display when the player types "off" command in the console
        public static string OffMessege()
        {
            return "GAME OVER";
        }

        //This function lets the player know that thier answer is correct
        public static string WellDoneMessage()
        {
            return "Well Done! your answer is correct";
        }

        //This function only draws a line when it is called
        public static string DrawLine()
        {
            return "====================================";
        }

        //This message is displayed when ever the program wants the player input
        public static string GetInputMessage() 
        {
            return "Please try guessing the number:";
        }

        public static string UnrecognizedInput() 
        {
            return "Sorry unrecognized input";
        }

    }
}
       
    

    
