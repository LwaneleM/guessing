﻿using System;

namespace GuessingGame
{
    
    public class DefaultRandomNumberGenerator : IRandomNumberGenerator
    {
        private readonly Random _random = new();

        //This functions genarate a number between min and max range and returns it
        public int GetRandomNumber(int min, int max)
        {
            return _random.Next(min, max);    
        }
    }
}
