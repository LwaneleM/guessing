﻿using System;

namespace GuessingGame
{
    public class Game : IGame
    {
        private readonly IRandomNumberGenerator _randomNumberGenerator;
       
        //This is a private setter and getter for guess attempts
        public int RemainingGuessAttempts { get; private set; }

        public int FinalAnswer { get; private set; }

        //This is a private setter and getter for checking if the game is completed
        public bool IsGameCompleted { get; private set; }

        //This is a private setter and getter for hint attempts 
        public int RemainingHintAttempts { get; private set; }

        public Game(IRandomNumberGenerator randomNumberGenerator)
        {
            _randomNumberGenerator = randomNumberGenerator;
        }

        public void RestoreGameState(int remainingGuessing, int remainingHintAttempts,
            int finalAnswer) 
        {
            RemainingGuessAttempts = remainingGuessing;
            RemainingHintAttempts = remainingHintAttempts;
            FinalAnswer = finalAnswer;
        }

        //This function initialise the fields in this class
        public void Initialise(int min, int max, int numberOfAttempts, int numberOfHints)
        {
            RemainingGuessAttempts = numberOfAttempts;
            RemainingHintAttempts = numberOfHints;
            FinalAnswer = _randomNumberGenerator.GetRandomNumber(min, max);
            
        }

        public bool IsPlaying => HasGuesses() && IsGameCompleted != true;


        //This function checks if the player still has attempts or not and its returns a boolean
        public bool HasGuesses()
        {
            return RemainingGuessAttempts != 0;
        }

        public AnswerState Guess(int answer)
        {
            if (HasGuesses())
            {
                RemainingGuessAttempts--;
                if (answer == FinalAnswer)
                {
                    IsGameCompleted = true;
                    return AnswerState.EQUAL;
                }
                else if (answer < FinalAnswer)
                {
                    IsGameCompleted = false;
                    return AnswerState.LOW;
                }
                else
                {
                    IsGameCompleted = false;
                    return AnswerState.HIGH;
                }
            }
            else
            {
                throw new InvalidOperationException("You have no attempts left");
            }
        }

        //This function will return true if there are hint remaining
       public bool HasHints()
       {
            return RemainingHintAttempts != 0;
       }

        //This function gives the player a hint about the answer
        public string GetHint()
        {

            if (HasHints())
            {
                RemainingHintAttempts--;
                //RemainingGuessAttempts = HasGuesses() ? RemainingGuessAttempts-- : RemainingGuessAttempts;
                if (HasGuesses()) {
                    RemainingGuessAttempts--;
                 }

                if ((RemainingHintAttempts % 2) == 0)
                {
                    return IsEvenOrOdd();
                }
                else
                {
                    return IsPrime();
                }
            }
            else
            {
                throw new InvalidOperationException("Sorry you have no hints left");
            }
        }

        public void ResetGame() 
        {
            RemainingGuessAttempts = 0;
            RemainingHintAttempts = 0;
            FinalAnswer = 0;
        }

        private string IsEvenOrOdd()
        {
            if ((FinalAnswer % 2) == 0)
            {
                return "The answer is an Even number";
            }
            else
            {
                return "The answer is an Odd number";
            }
        }

        //This function will check the answer if a prime number or not
        private string IsPrime()
        {
            int prime = 0;
            for (int count = 1; count <= FinalAnswer; count++)
            {
                if ((FinalAnswer % count) == 0)
                {
                    prime++;
                }
            }
            if (prime == 2)
            {
                return "The answer is a prime number";
            }
            else
            {
                return "The answer is not a prime number";
            }
        }

    }
}
