﻿namespace GuessingGame
{
    public enum InputResponse
    {
        NUMBER,
        INVALDINPUT,
        HINT,
        HELP,
        EXIT
    }
}
