﻿using GuessingGame.Console;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Threading.Tasks;

namespace GuessingGame
{
    public class Program
    {
        // This Function plays the game and all the functions or methods are called in it 
        static async Task Main()
        {
            using var host = CreateHostBuilder().Build();
            {
                await host.StartAsync();
                  
                await host.StopAsync();
            }
        }

        private static IHostBuilder CreateHostBuilder()
        {
            return Host.CreateDefaultBuilder()
                .ConfigureServices(service =>
                {
                    service.AddSingleton<DefaultRandomNumberGenerator>();
                    service.AddSingleton<IRandomNumberGenerator>(c =>
                    {
                        return new DebugRandomNumberGenerator(c.GetService<DefaultRandomNumberGenerator>());
                    });
                    service.AddSingleton<IGame, Game>();
                    service.AddTransient<IHostedService, GameHostedService>();
                });
        }

    }
}
