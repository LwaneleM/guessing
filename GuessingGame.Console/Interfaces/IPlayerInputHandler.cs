﻿namespace GuessingGame
{
    public interface IPlayerInputHandler
    {
        public PlayerInputHandlerResult Handle(string playerInput);
    }
}

