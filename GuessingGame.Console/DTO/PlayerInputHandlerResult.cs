﻿
namespace GuessingGame
{
    public class PlayerInputHandlerResult
    {
        public string Message { get; set; }

        public bool IsStillPlaying { get; set; }
    }
}
