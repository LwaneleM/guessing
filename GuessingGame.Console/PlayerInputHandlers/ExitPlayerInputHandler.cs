﻿using System;

namespace GuessingGame
{
    public class ExitPlayerInputHandler : IPlayerInputHandler
    {
        public PlayerInputHandlerResult Handle(string playerInput)
        {
            return new PlayerInputHandlerResult
            {
                Message = UserFeedback.OffMessege(),
                IsStillPlaying = false
            };
        }
    }
}
