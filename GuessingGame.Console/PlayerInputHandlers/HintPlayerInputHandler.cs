﻿namespace GuessingGame
{
    public class HintPlayerInputHandler : IPlayerInputHandler
    {
        private readonly IGame _gameState;
 
        public HintPlayerInputHandler(IGame gameState)
        {
            _gameState = gameState;
        }
        public string HintMessage()
        { 
            if (_gameState.HasHints())
            {
                return _gameState.GetHint();
            }
            else
            {
                return "Sorry you don't have enough hints";
            }
        }

        public PlayerInputHandlerResult Handle(string playerInput)
        {
            return new PlayerInputHandlerResult
            {
                IsStillPlaying = true,
                Message = $"{UserFeedback.DrawLine()}\n{UserFeedback.GameInfo(_gameState.RemainingHintAttempts, _gameState.RemainingGuessAttempts)}" +
                                  $"\n{HintMessage()}",
                
            };
        }
    }
}
