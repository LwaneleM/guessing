﻿namespace GuessingGame
{
    public class InvalidPlayerInputHandler : IPlayerInputHandler
    {
        public PlayerInputHandlerResult Handle(string playerInput)
        {
            return new PlayerInputHandlerResult
            {
                Message = UserFeedback.UnrecognizedInput() + "\n"+UserFeedback.HelpMessage(),
                IsStillPlaying = true
            };
        }
    }
}
