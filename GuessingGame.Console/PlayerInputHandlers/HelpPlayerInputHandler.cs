﻿using System;

namespace GuessingGame.Console
{
    public class HelpPlayerInputHandler : IPlayerInputHandler
    {
        public PlayerInputHandlerResult Handle(string playerInput)
        {
            return new PlayerInputHandlerResult()
            {
                Message = UserFeedback.HelpMessage(),
                IsStillPlaying = true
            };
        }

       
    }
}
