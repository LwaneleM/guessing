﻿using System;

namespace GuessingGame
{
    public class ValidateUserInput
    {
        public static InputResponse IsAnswerValid(string playerInput)
        {
            if (int.TryParse(playerInput, out _))
            {
                return InputResponse.NUMBER;
            }
            else if (playerInput.Equals("hint", StringComparison.InvariantCultureIgnoreCase))
            {
                return InputResponse.HINT;
            }
            else if (playerInput.Equals("exit", StringComparison.InvariantCultureIgnoreCase))
            {
                return InputResponse.EXIT;
            }
            else if (playerInput.Equals("help", StringComparison.InvariantCultureIgnoreCase))
            {
                return InputResponse.HELP;
            }
            else 
            {
                return InputResponse.INVALDINPUT;
            }
            
        }
    }
}
