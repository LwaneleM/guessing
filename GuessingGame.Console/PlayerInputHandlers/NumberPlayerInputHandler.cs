﻿using System;

namespace GuessingGame
{
    public class NumberPlayerInputHandler : IPlayerInputHandler
    {
        private readonly IGame _game;
    
        public NumberPlayerInputHandler(IGame game)
        {
            _game = game;
        }

        public PlayerInputHandlerResult Handle(string playerInput)
        {
            return new PlayerInputHandlerResult
            {
                Message = CheckGuess(playerInput),
                IsStillPlaying = true
            };
        }

        private string CheckGuess(string playerInput)
        {

            return _game.Guess(int.Parse(playerInput)) switch
            {
                AnswerState.EQUAL => $"{UserFeedback.DrawLine()}\n{UserFeedback.GameInfo(_game.RemainingHintAttempts, _game.RemainingGuessAttempts)}" +
                                     $"\n{UserFeedback.WellDoneMessage()}",

                AnswerState.LOW => $"{UserFeedback.DrawLine()}\n{UserFeedback.GameInfo(_game.RemainingHintAttempts, _game.RemainingGuessAttempts)}" +
                                   $"\n{UserFeedback.TooLowMessage()}",

                AnswerState.HIGH => $"{UserFeedback.DrawLine()}\n{UserFeedback.GameInfo(_game.RemainingHintAttempts, _game.RemainingGuessAttempts)}" +
                                  $"\n{UserFeedback.TooHighMessage()}",

                _ => throw new InvalidOperationException("Invalid command"),
            };
        }
    }
}
