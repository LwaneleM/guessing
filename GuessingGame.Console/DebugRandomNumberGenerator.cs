﻿using System;

namespace GuessingGame
{
    public class DebugRandomNumberGenerator : IRandomNumberGenerator
    {
        private readonly IRandomNumberGenerator _randomNumberGenerator;

        public DebugRandomNumberGenerator(IRandomNumberGenerator randomNumberGenerator)
        {
            _randomNumberGenerator = randomNumberGenerator;
           

        }

        public int GetRandomNumber(int min, int max)
        {
            var randomNumber = _randomNumberGenerator.GetRandomNumber(min, max);
            System.Console.WriteLine("***************Debug answer****************");
            System.Console.WriteLine("----" + randomNumber);
            System.Console.WriteLine("*******************************************");

            return randomNumber;
        }
    }
}
