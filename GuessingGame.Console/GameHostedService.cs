﻿using System.Threading.Tasks;
using System.Threading;
using Microsoft.Extensions.Hosting;
using System.Collections.Generic;

namespace GuessingGame.Console
{
    public class GameHostedService : IHostedService
    {
        private readonly IGame _game;
        public GameHostedService(IGame game)
        {
            _game = game;
        }

        //This function will get some userInput and return it as a string
        public static string GetUserInput()
        {
            string userInput = System.Console.ReadLine();
            while (true)
            {
                if (userInput.Length <= 0)
                {
                    PrintMessage("Please Type something!");
                    userInput = System.Console.ReadLine();
                }
                return userInput;
            }
        }

        //This function is only responsible for displayin messages in the screen
        public static void PrintMessage(string message)
        {
           System.Console.WriteLine(message);
        }

        //This Dictionary is responsible for creating Handler objects
        public static Dictionary<InputResponse, IPlayerInputHandler> CreatePlayerInputHandlers(IGame game)
        {
            var gameInputHandler = new Dictionary<InputResponse, IPlayerInputHandler>
            {
                [InputResponse.HINT] = new HintPlayerInputHandler(game),
                [InputResponse.HELP] = new HelpPlayerInputHandler(),
                [InputResponse.EXIT] = new ExitPlayerInputHandler(),
                [InputResponse.INVALDINPUT] = new InvalidPlayerInputHandler(),
                [InputResponse.NUMBER] = new NumberPlayerInputHandler(game)
            };
            return gameInputHandler;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            System.Console.WriteLine(UserFeedback.IntroMessege());

            _game.Initialise(1, 100, 5, 3);
            bool isStillPLaying = true;
            var gameInputHandlers = CreatePlayerInputHandlers(_game);

            while (_game.IsPlaying && isStillPLaying)
            {
                PrintMessage($"{UserFeedback.DrawLine()}\n{UserFeedback.GetInputMessage()}");
                var playerAnswer = GetUserInput();
                var userInput = ValidateUserInput.IsAnswerValid(playerAnswer);

                if (gameInputHandlers.TryGetValue(userInput, out var inputHandler))
                {
                    var playerInputHandlerResult = inputHandler.Handle(playerAnswer); 
                    isStillPLaying = playerInputHandlerResult.IsStillPlaying;
                    PrintMessage(playerInputHandlerResult.Message);
                }
            }
            if(_game.IsPlaying == false){
                PrintMessage(UserFeedback.OffMessege());
            }
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}
