﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using GuessingGame.WebsiteMVC.Models;

namespace GuessingGame.WebsiteMVC.Data
{
    public class GuessingGameWebsiteMVCContext : DbContext
    {
        public GuessingGameWebsiteMVCContext (DbContextOptions<GuessingGameWebsiteMVCContext> options)
            : base(options)
        {
        }

        public DbSet<Movie> Movie { get; set; }
    }
}
