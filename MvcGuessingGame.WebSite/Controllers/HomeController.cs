﻿using Microsoft.AspNetCore.Mvc;
using GuessingGame.WebSite.Mvc.Models;
using GuessingGame;
using GuessingGame.Application;
using System;
using Microsoft.AspNetCore.Http;

namespace MvcMovie.Controllers
{
    public class HomeController : Controller
    {
      
        private readonly IGameRepository _gameRepository;
        private readonly string _gameIdSessionKey = "gameId";

        public HomeController(IGameRepository game)
        { 
            _gameRepository = game;
        }

        public IActionResult Index()
        {
            var game = GetGameState();
            
            var model = new GameModel
            {
                NumberOfGuesses = game.RemainingGuessAttempts,
                NumberOfHints = game.RemainingHintAttempts,
                MessageTitle = "Game :",
                Message = "Number has been generated. Try Guessing it!"
            };
            _gameRepository.SaveGame(Guid.Parse(HttpContext.Session.GetString(_gameIdSessionKey)), game);

            return View(model);
        }

        public IActionResult Guess() 
        {
            var game = GetGameState();
            string message = GetGuessMessage(game);
     
            var model = new GameModel
            {
                NumberOfGuesses = game.RemainingGuessAttempts,
                NumberOfHints = game.RemainingHintAttempts,
                MessageTitle = "Guess :",
                Message = message
            };

            _gameRepository.SaveGame(Guid.Parse(HttpContext.Session.GetString(_gameIdSessionKey)), game);

            return View("Index",model);
        }

        public IActionResult Hint() 
        {
            var game = GetGameState();
            var message = GetHintMessage(game);
            var model = new GameModel
            {
                NumberOfGuesses = game.RemainingGuessAttempts,
                NumberOfHints = game.RemainingHintAttempts,
                MessageTitle = "Hint :",
                Message = message
            };
            _gameRepository.SaveGame(Guid.Parse(HttpContext.Session.GetString(_gameIdSessionKey)), game);

            return View("Index",model);
        }

        public IActionResult Restart() 
        {
            _gameRepository.RestartGame(Guid.Parse(HttpContext.Session.GetString(_gameIdSessionKey)));
            var game = GetGameState();

            var model = new GameModel
            {
                NumberOfGuesses = game.RemainingGuessAttempts,
                NumberOfHints = game.RemainingHintAttempts,
                MessageTitle = "Game :",
                Message = "The Game has restarted successfully"
            };
            _gameRepository.SaveGame(Guid.Parse(HttpContext.Session.GetString(_gameIdSessionKey)), game);

            return View("Index",model);
        }

        private static string GetHintMessage(IGame game) 
        {
            if (game.HasHints())
            {
                return game.GetHint();
            }
            else
            {
                return UserFeedback.NoHintsLeft();
            }
        }

        private IGame GetGameState()
        {
            if (string.IsNullOrEmpty(HttpContext.Session.GetString(_gameIdSessionKey)))
            {
                var gameData = _gameRepository.CreateGame();
                base.HttpContext.Session.SetString(_gameIdSessionKey, gameData.Item1.ToString());
                return gameData.Item2;
            }
            else
            {
                var gameId = _gameRepository.GetGame(Guid.Parse(HttpContext.Session.GetString(_gameIdSessionKey)));
                return gameId;
            }
        }

        private string GetGuessMessage(IGame game)
        {
            if (game.HasGuesses())
            {
                string playerGuess = Request.Form["getInput"];
                return FormInputHandler.CheckUserInput(ValidateUserInput.IsAnswerValid(playerGuess), game);
            }
            else
            {
                game.ResetGame();
                return UserFeedback.OffMessege();
            }
        }
    }
}
