﻿namespace GuessingGame.WebSite.Mvc.Models
{
    public class GameModel
    {
        public int NumberOfHints { get; set; }
        public int NumberOfGuesses { get; set; }
        public string MessageTitle { get; set; }
        public string Message { get; set; }
    }
}
