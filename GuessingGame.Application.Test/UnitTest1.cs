using NSubstitute;
using System;
using Xunit;

namespace GuessingGame.Application.Test
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            //Arrange
            var mockRandomNumber = Substitute.For<IRandomNumberGenerator>();
            var game = new Game(mockRandomNumber);
            var InMemoryGameRepository = Substitute.For<InMemoryGameRepository>(mockRandomNumber);
            var id = new Guid();
            game.Initialise(1, 100, 5, 3);

            InMemoryGameRepository.CreateGame().Returns((id,game));
            var result = InMemoryGameRepository.CreateGame();

            Assert.Equal(result.Item1, id);
            Assert.Equal(result.Item2,game);
        }

    }
}
