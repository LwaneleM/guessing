﻿using Xunit;
namespace GuessingGame.Tests
{
    public class ExitInputTest
    {
        [Fact]
        public void ItShouldDisplayExitingMessage()
        {
            //Arrange
            var expectedOutPut = "GAME OVER";
            var executeExit = new ExitPlayerInputHandler();

            //act
            var actualOutPut = executeExit.Handle("").Message;

            //Assert
            Assert.Equal(expectedOutPut, actualOutPut);
        }
    }
}
