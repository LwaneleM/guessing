﻿using NSubstitute;
using Xunit;
namespace GuessingGame.Tests
{
   
    public class NumberInputTest
    {
        [Fact]
        public void ItShouldSayTooHigh() 
        {
            //Arrange
            var randomNumber = Substitute.For<IRandomNumberGenerator>();
            var game = new Game(randomNumber);
            var expectedOutput = "====================================\nHint : 2\nAttempts : 3\nSorry your guess is too high";
            var executeNumberInput = new NumberPlayerInputHandler(game);

            //Act
            randomNumber.GetRandomNumber(Arg.Any<int>(), Arg.Any<int>()).Returns(4);
            game.RestoreGameState(4, 2, 4);
            var actualAnswer = executeNumberInput.Handle("10");

            //Assert
            Assert.Equal(expectedOutput, actualAnswer.Message);
        }

        [Fact]
        
        // TODO: Clean up test descriptions
        public void ItShouldSayTooLow() 
        {
            //Arrange
            var randomNumber = Substitute.For<IRandomNumberGenerator>();
            string mockPlayerInput = "3";
            var game = new Game(randomNumber);
            var expectedOutput = "====================================\nHint : 2\nAttempts : 3\nSorry your guess is too low";
            var executeNumberInput = new NumberPlayerInputHandler(game);

            //Act
            randomNumber.GetRandomNumber(Arg.Any<int>(), Arg.Any<int>()).Returns(5);
            game.RestoreGameState(4,2,5);
            var actualAnswer = executeNumberInput.Handle(mockPlayerInput);

            //Assert
            Assert.Equal(expectedOutput, actualAnswer.Message);
        }


    }
}
