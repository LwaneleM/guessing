using Xunit;

namespace GuessingGame.Tests
{
    public class UserFeedbackTests
    {
        [Fact]
        public void WhenItsCalledItsItsShouldDisplayIntroMessage()
        {
            // Arrange
            string expectedResult = "=================IT'S PLAY TIME================== \n" +
                "Hint: 2\nAttempts: 4\nLet the games begin!";

            // Act
            string actualResult = UserFeedback.IntroMessege();

            // Assert
            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void ItShouldDisplayTheHelpMessage_WhenItIsCalled()
        {
            //Arrange
            string expectedResult = "== Commands I understand : \n" +
                         "> exit - Shutdown the game\n" +
                         "> Hint - Gives you the hint about the answer\n" +
                         "> Number e.g {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10��}\n";

            //Act
            string actualResult = UserFeedback.HelpMessage();
            
            //Assert
            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void ItsShouldDisplayWhenThereAreNoHintsLeft()
        {
            //Arrange
            string expectedResult = "Sorry there are no hints left";
            
            //Act
            string actualResult = UserFeedback.NoHintsLeft();
            
            //Assert
            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void ItsShouldDisplayAMessageIfYourAnswerIsGreaterThenTheCorrectAnswer()
        {
            //Arrange
            string expectedResult = "Sorry your guess is too high";
            
            //Act
            string actualResult = UserFeedback.TooHighMessage();
            
            //Assert
            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void ItShouldDisplayTheAttemptsAndHintLeft()
        {
            //Arrange
            string expectedResult = $"Hint : {2}\nAttempts : {4}";
            
            //Act
            string actualResult = UserFeedback.GameInfo(2, 4);
            
            //Assert
            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void ItsShouldDisplayAMessageIfYourAnswerIsLessThenTheCorrectAnswer()
        {
            //Arrange
            string expectedResult = "Sorry your guess is too low";

            //Act
            string actualResult = UserFeedback.TooLowMessage();

            //Assert
            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void ItShouldDisplayAMessage_IfTheInputIsOff()
        {
            //Arrange
            string expectedResult = "GAME OVER";
            
            //Act
            string actualResult = UserFeedback.OffMessege();
           
            //Assert
            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void ItWillDisplayAMessageWhenTheAnswerIsCorrcet()
        {
            //Arrange
            string expectedResult = "Well Done! your answer is correct";
            
            //Act
            string actualResult = UserFeedback.WellDoneMessage();
            
            //Assert
            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void ItShouldDrawALineWhenItsIsCalled() {
            //Arrange
            string expectedResult = "====================================";
          
            //Act
            string actualResult = UserFeedback.DrawLine();
            
            //Assert
            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
         public void ItsShouldDisplayWhenWeWantToGetUserInput() 
         {
            //Arrange
            string expectedResult = "Please try guessing the number:";
            
            //Act
            string actualResult = UserFeedback.GetInputMessage();
            
            //Assert
             Assert.Equal(expectedResult, actualResult);
         }
    }
}