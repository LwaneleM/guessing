﻿using Xunit;

namespace GuessingGame.Tests
{
    public class MockRandomNumberGenerator : IRandomNumberGenerator 
    {
        private readonly int _mockInput;
        public MockRandomNumberGenerator(int mockInput) 
        {
            _mockInput = mockInput;
        }

        public int GetRandomNumber(int min, int max)
        {
            return _mockInput;
        }
    }
    public class HintInputTest
    {
        [Fact]
        public void ItShouldSayTheNUmberIsEven()
        {
            //Arrange
            var expectedResult = "====================================\nHint : 3\nAttempts : 5\nThe answer is an Even number";
            var game = new Game(new MockRandomNumberGenerator(8));
            game.Initialise(0, 2, 5, 3);
            var executeHelp = new HintPlayerInputHandler(game);
            
            //Act
            var actual = executeHelp.Handle("").Message;
            
            //Assert
            Assert.Equal(expectedResult, actual);
        }

        [Fact]
        public void ItShouldSayTheNUmberIsOdd()
        {
            //Arrange
            var expectedResult = "====================================\nHint : 3\nAttempts : 5\nThe answer is an Odd number";
            var game = new Game(new MockRandomNumberGenerator(7));
            game.Initialise(0, 2, 5, 3);
            var executeHelp = new HintPlayerInputHandler(game);

            //Act
            var actual = executeHelp.Handle("").Message;

            //Assert
            Assert.Equal(expectedResult, actual);
        }

        [Fact]
        public void ItShouldSayTheNUmberIsPrime()
        {
            //Arrange
            
            var expectedResult = "====================================\nHint : 4\nAttempts : 5\nThe answer is a prime number";
            var game = new Game(new MockRandomNumberGenerator(7));
            game.Initialise(0, 2, 5, 4);
            var executeHelp = new HintPlayerInputHandler(game);

            //Act
            var actual = executeHelp.Handle("").Message;

            //Assert
            Assert.Equal(expectedResult, actual);
        }
    }
}
