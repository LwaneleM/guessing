﻿using Xunit;

namespace GuessingGame.Tests
{
    public class DefaultRandomNumberGeneratorTests
    {
        [Fact]
        public static void ItShouldGenerateRandomNumber() 
        {
            // Arrange
            int min = 10;
            int max = 50;
            var randomNumberGenerator = new DefaultRandomNumberGenerator();

            // Act
            var result = randomNumberGenerator.GetRandomNumber(min, max);

            // Assert
            Assert.InRange(result, min, max);
        }
    }
}
