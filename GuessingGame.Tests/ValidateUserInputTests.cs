﻿using Xunit;

namespace GuessingGame.Tests
{
    public class ValidateUserInputTests
    {
        [Fact]
        public void ItShouldReturnAnEnumNamedNumber()
        {
            //Arrange
            var mockInput = "3";
            InputResponse expectedOutput = InputResponse.NUMBER;

            //Act
            var actualOutput = ValidateUserInput.IsAnswerValid(mockInput);
            
            //Assert
            Assert.Equal(expectedOutput,actualOutput);
        }

        [Fact]
        public void ItShouldReturnAnEnumNamedHint()
        {
            //Arrange
            var mockInput = "Hint";
            InputResponse expectedOutput = InputResponse.HINT;

            //Act
            var actualOutput = ValidateUserInput.IsAnswerValid(mockInput);

            //Assert
            Assert.Equal(expectedOutput, actualOutput);
        }

        [Fact]
        public void ItShouldReturnAnEnumNamedInvalid()
        {
            //Arrange
            var mockInput = "e";
            InputResponse expectedOutput = InputResponse.INVALDINPUT;

            //Act
            var actualOutput = ValidateUserInput.IsAnswerValid(mockInput);

            //Assert
            Assert.Equal(expectedOutput, actualOutput);
        }

        [Fact]
        public void ItShouldReturnAnEnumNamedExit() 
        {
            //Arrange
            var mockInput = "eXit";
            InputResponse expectedOutput = InputResponse.EXIT;

            //Act
            var actualOutput = ValidateUserInput.IsAnswerValid(mockInput);

            //Assert
            Assert.Equal(expectedOutput, actualOutput);

        }

        [Fact]
        public void ItShouldReturnAnEnumNamedHelp() 
        {
            //Arrange
            var mockInput = "Help";
            InputResponse expectedOutput = InputResponse.HELP;

            //Act
            var actualOutput = ValidateUserInput.IsAnswerValid(mockInput);

            //Assert
            Assert.Equal(expectedOutput, actualOutput);
        }


    }
}
