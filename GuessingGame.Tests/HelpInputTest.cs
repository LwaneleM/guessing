﻿using GuessingGame.Console;
using Xunit;
namespace GuessingGame.Tests
{
    public class HelpInputTest
    {
        [Fact]
        public void ItShouldDisplayMessageWithAllowedCommands() 
        {
            //Arrange
            var executeHelp = new HelpPlayerInputHandler();
            var expectedResult = "== Commands I understand : \n" +
                         "> exit - Shutdown the game\n" +
                         "> Hint - Gives you the hint about the answer\n" +
                         "> Number e.g {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10……}\n";

            //Act
            var actual = executeHelp.Handle("").Message;

            //Assert
            Assert.Equal(expectedResult, actual);

        }
    }
}
