﻿using Xunit;

namespace GuessingGame.Tests
{
    public class GameTest
    {
        public class MockRandomNumberGenerator : IRandomNumberGenerator
        {
            private readonly int mockNumber; 
            public MockRandomNumberGenerator(int mockInput) {
                mockNumber = mockInput;
            }
            public int GetRandomNumber(int min, int max)
            {
                return mockNumber;
            }
        }

        [Fact]
        public void ItsShouldReturnTheRightAnswer()
        {
            //Arrange
            var randomNumberGenerator = new MockRandomNumberGenerator(55);
            var gameState = new Game(randomNumberGenerator);
            gameState.Initialise(20, 55, 4, 2);
            var expectedAnswer = AnswerState.EQUAL;
            
            //Act
            var actualAnswer = gameState.Guess(55);

            //Assert
            Assert.Equal(expectedAnswer,actualAnswer);
        }

        [Fact]
        public void WhenThereAreNoAttemptsRemaining_ItsShouldReturnFalse()
        {
            //Arrange
            var randomNumberGenerator = new MockRandomNumberGenerator(55);
            var gameState = new Game(randomNumberGenerator);
            gameState.Initialise(2, 10, 4, 2);
            var expectedAnswer = AnswerState.LOW;

            //Act
            var actualAnswer = gameState.Guess(20);

            //Assert
            Assert.Equal(expectedAnswer,actualAnswer);
        }

        [Fact]
        public void WhenThereAreAttemptsRemaining_ItShouldReturnTrue()
        {
            //Arrange
            var randomNumberGenerator = new MockRandomNumberGenerator(55);
            var gameState = new Game(randomNumberGenerator);
            gameState.Initialise(2, 15, 4, 3);
            
            //Act
            var actualAnswer = gameState.HasGuesses();
            
            //Assert
            Assert.True(actualAnswer);
        }

        [Fact]
        public void WhenThereAreNoAttemptsRemaining_ItShouldReturnFalse()
        {
            //Arrange
            var randomNumberGenerator = new MockRandomNumberGenerator(55);
            var gameState = new Game(randomNumberGenerator);
            gameState.Initialise(2, 15, 40, 2);

            //Act
            var actualAnswer = gameState.HasGuesses();

            //Assert
            Assert.True(actualAnswer);
        }

        [Fact]
        public void WhenThereAreHintAttemptsRemaining_ItsShouldReturnTrue()
        {
            //Arrange
            var randomNumberGenerator = new MockRandomNumberGenerator(55);
            var gameState = new Game(randomNumberGenerator);
            gameState.Initialise(2, 15, 10, 2);

            //Act
            var actualaAnswer = gameState.HasHints();
            //Assert
            Assert.True(actualaAnswer);
        }

        [Fact]
        public void WhenThereAreNoHintAttemptsRemaining_ItsShouldReturnFalse()
        {
            //Arrange
            var randomNumberGenerator = new MockRandomNumberGenerator(55);
            var gameState = new Game(randomNumberGenerator);
            gameState.Initialise(2, 15, 10, 0);

            //Act
            var actualaAnswer = gameState.HasHints();

            //Assert
            Assert.False(actualaAnswer);
        }

        [Fact]
        public void ItShouldSayTheNumberIsAnEvenNumber()
        {
            //Arrange
            var randomNumberGenerator = new MockRandomNumberGenerator(20);
            var gameState = new Game(randomNumberGenerator);
            gameState.Initialise(2, 15, 10, 3);
            var expactedAnswer = "The answer is an Even number";
            
            //Act
            var actualaAnswer = gameState.GetHint();

            //Assert
            
            Assert.Equal(expactedAnswer,actualaAnswer);
        }
        
        [Fact]
        public void ItShouldSayTheNumberIsAnOddNumber()
        {
            //Arrange
            var randomNumberGenerator = new MockRandomNumberGenerator(5);
            var gameState = new Game(randomNumberGenerator);
            gameState.Initialise(2, 15, 10, 3);
            var expactedAnswer = "The answer is an Odd number";
            
            //Act
            var actualaAnswer = gameState.GetHint();

            //Assert
            Assert.Equal(expactedAnswer, actualaAnswer);
        }

        [Fact]
        public void ItShouldSayTheNumberIsAPrimeNumber()
        {
            //Arrange
            var randomNumberGenerator = new MockRandomNumberGenerator(7);
            var gameState = new Game(randomNumberGenerator);
            gameState.Initialise(2, 15, 10, 8);
            var expactedAnswer = "The answer is a prime number";
            
            //Act
            var actualaAnswer = gameState.GetHint();

            //Assert
            Assert.Equal(expactedAnswer, actualaAnswer);
        }
    }
}
