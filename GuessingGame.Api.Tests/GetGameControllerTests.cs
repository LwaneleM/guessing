﻿using Xunit;
using GuessingGame.Application.Api.Controllers.GetGame;
using NSubstitute;
using GuessingGame.Application;
using System;

namespace GuessingGame.Api.Tests
{
    public class GetGameControllerTests
    {
        [Fact]
        public void ItShouldReturnGameState()
        {
            //Arrange
            var mockRandomNumber = Substitute.For<IRandomNumberGenerator>();
            var game = new Game(mockRandomNumber);
            var InMemoryGameRepository = Substitute.For<IGameRepository>();
            var getGameController = Substitute.For<GetGameController>(InMemoryGameRepository);
            var expectedAnswer = new GetGameResult
            {
                GuessesRemaining = 8,
                HintsRemaining = 7,
                IsPlaying = true
            };

            //Act
            game.RestoreGameState(8, 7, 8);
            InMemoryGameRepository.GetGame(Arg.Any<Guid>()).Returns(game);
            var result = getGameController.Execute(new Guid());
            var actualAnswer = result.Value;

            //Assert
            Assert.Equal(expectedAnswer.IsPlaying, actualAnswer.IsPlaying);
            Assert.Equal(expectedAnswer.GuessesRemaining, actualAnswer.GuessesRemaining);
            Assert.Equal(expectedAnswer.HintsRemaining, actualAnswer.HintsRemaining);
        }
    }
}
