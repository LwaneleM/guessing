﻿using GuessingGame.Application;
using GuessingGame.Application.Api.Controllers.RestartGame;
using Microsoft.AspNetCore.Mvc;
using NSubstitute;
using System;
using Xunit;

namespace GuessingGame.Api.Tests
{
    public class RestartGameTests
    {
        [Fact]
        public void ItShouldReturnOkStatus()
        {
            //Arrange
            var mockRandomNumber = Substitute.For<IRandomNumberGenerator>();
            var game = new Game(mockRandomNumber);
            var InMemoryGameRepository = Substitute.For<IGameRepository>();
            var restartGameController = new RestartController(InMemoryGameRepository);

            //Act
            game.RestoreGameState(5, 6, 8);
            var actualAnswer = restartGameController.RestartGame(new Guid());
            //Assert
            Assert.IsAssignableFrom<OkResult>(actualAnswer); 
        }
    }
    
}
