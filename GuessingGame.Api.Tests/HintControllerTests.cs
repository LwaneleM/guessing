using GuessingGame.Application;
using GuessingGame.Application.Api.Controllers.Hint;
using NSubstitute;
using System;
using Xunit;

namespace GuessingGame.Api.Tests
{
    public class HintControllerTests
    {
        
        [Fact]
        public void ItShouldReturnAnswerIsEven()
        {
            //Arrange
            var gameId = Guid.NewGuid();
            var randomNumber = Substitute.For<IRandomNumberGenerator>();
            var InMemoryGameRepository = Substitute.For<IGameRepository>();
            var hintController = Substitute.For<HintController>(InMemoryGameRepository);
            randomNumber.GetRandomNumber(Arg.Any<int>(), Arg.Any<int>()).Returns(5);
            var expected = "The answer is an Even number";

            //Act
            Game game = new(randomNumber);
            game.RestoreGameState(4, 3, 4);
            InMemoryGameRepository.GetGame(Arg.Any<Guid>()).Returns(game);
            var res = hintController.Execute(gameId);
            //Assert
            Assert.Equal(expected, res.Value.HintMessage);
        }

        [Fact]
        public void ItShouldReturnAnswerIsOdd() 
        {
            //Arrange
            var gameId = Guid.NewGuid();
            var randomNumber = Substitute.For<IRandomNumberGenerator>();
            var InMemoryGameRepository = Substitute.For<IGameRepository>();
            var hintController = Substitute.For<HintController>(InMemoryGameRepository);
            randomNumber.GetRandomNumber(Arg.Any<int>(), Arg.Any<int>()).Returns(5);
            var expected = "The answer is an Odd number";
            Game game = new(randomNumber);

            //Act
            game.RestoreGameState(4, 3, 5);
            InMemoryGameRepository.GetGame(Arg.Any<Guid>()).Returns(game);
            var message = hintController.Execute(gameId).Value.HintMessage;

            //Assert
            Assert.Equal(expected,message);
        }

        [Fact]
        public void ItShouldReturnAnswerIsPrime()
        {
            //Arrange
            var gameId = Guid.NewGuid();
            var randomNumber = Substitute.For<IRandomNumberGenerator>();
            var InMemoryGameRepository = Substitute.For<IGameRepository>();
            var hintController = Substitute.For<HintController>(InMemoryGameRepository);
            randomNumber.GetRandomNumber(Arg.Any<int>(), Arg.Any<int>()).Returns(5);
            var expected = "The answer is a prime number";
        
            //Act
            Game game = new(randomNumber);
            game.RestoreGameState(4, 4, 5);
            InMemoryGameRepository.GetGame(Arg.Any<Guid>()).Returns(game);
            var message = hintController.Execute(gameId).Value.HintMessage;

            //Assert
            Assert.Equal(expected, message);
        }
    }
}
