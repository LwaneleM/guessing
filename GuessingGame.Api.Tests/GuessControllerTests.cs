﻿using GuessingGame.Application;
using GuessingGame.Application.Api.Controllers.Guess;
using NSubstitute;
using System;
using Xunit;

namespace GuessingGame.Api.Tests
{
    public class GuessControllerTests
    {
        [Fact]
        public void ItShouldReturnAnswerIsLow()
        {
            //Arrange
            var gameId = Guid.NewGuid();
            var randomNumber = Substitute.For<IRandomNumberGenerator>();
            var inMemoryGameRepository = Substitute.For<IGameRepository>();
            var guessController = new GuessController(inMemoryGameRepository);
            randomNumber.GetRandomNumber(Arg.Any<int>(), Arg.Any<int>()).Returns(5);
            var expected = AnswerState.LOW;
            var guessOutput = Substitute.For<GuessInput>();
            Game game = new(randomNumber);


            //Act
            game.RestoreGameState(4, 8, 8);
            inMemoryGameRepository.GetGame(Arg.Any<Guid>()).Returns(game);
            var message = guessController.GuessInputPost(gameId,guessOutput).Value.Message;

            //Assert
            Assert.Equal(expected, message);
        }

        [Fact]
        public void ItShouldReturnAnswerIsEqual()
        {
            //Arrange
            var gameId = Guid.NewGuid();
            var randomNumber = Substitute.For<IRandomNumberGenerator>();
            var inMemoryGameRepository = Substitute.For<IGameRepository>();
            var GuessController = new GuessController(inMemoryGameRepository);
            randomNumber.GetRandomNumber(Arg.Any<int>(), Arg.Any<int>()).Returns(5);
            var expected = AnswerState.EQUAL;
            var guessInput = new GuessInput
            {
                Answer = 5
            };
            Game game = new(randomNumber);

            //Act
            game.RestoreGameState(4, 8, 5);
            inMemoryGameRepository.GetGame(Arg.Any<Guid>()).Returns(game);
            var message = GuessController.GuessInputPost(gameId, guessInput).Value.Message;

            //Assert
            Assert.Equal(expected, message);
        }

        [Fact]
        public void ItShouldReturnAnswerIsHigh()
        {
            //Arrange
            var gameId = Guid.NewGuid();
            var randomNumber = Substitute.For<IRandomNumberGenerator>();
            var inMemoryGameRepository = Substitute.For<IGameRepository>();
            var guessController = new GuessController(inMemoryGameRepository);
            randomNumber.GetRandomNumber(Arg.Any<int>(), Arg.Any<int>()).Returns(5);
            var expected = AnswerState.HIGH;
            var guessInput = new GuessInput
            {
                Answer = 20
            }; ;
            Game game = new(randomNumber);


            //Act
            game.RestoreGameState(4, 8, 8);
            inMemoryGameRepository.GetGame(Arg.Any<Guid>()).Returns(game);
            var message = guessController.GuessInputPost(gameId, guessInput).Value.Message;

            //Assert
            Assert.Equal(expected, message);
        }
    }
}
